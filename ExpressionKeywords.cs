using System;
using System.Collections.Generic;

namespace Victor
{
	/// <summary>
	/// Functions analogous to keywords that usually wrap statements in a block.
	/// These functions wrap a single expression instead, and return its value.
	/// </summary>
	public static class ExpressionKeywords
	{
		public static T Lock<T>(object @lock, Func<T> expression)
		{
			lock (@lock)
				return expression();
		}

		public static TExpression Using<TDisposable, TExpression>(
			TDisposable disposable, Func<TDisposable, TExpression> expression)
			where TDisposable : IDisposable
		{
			using (disposable)
				return expression(disposable);
		}

		public static TExpression Try<TExpression, TException>(
			Func<TExpression> expression, Func<TException, TExpression> exceptionHandler)
			where TException : Exception
		{
			try
			{
				return expression();
			}
			catch (TException e)
			{
				return exceptionHandler(e);
			}
		}

		public static IEnumerable<T> While<T>(Func<bool> condition, Func<T> expression)
		{
			while (condition())
				yield return expression();
		}

		public static IEnumerable<T> DoWhile<T>(Func<T> expression, Func<T, bool> condition)
		{
			T t;
			do
			{
				t = expression();
				yield return t;
			}
			while (condition(t));
		}
	}
}
