using System;
using System.Runtime.CompilerServices;

namespace Victor.Extensions
{
	public static class GeneralExtensions
	{
		/// <summary>
		/// Does nothing.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Ignore<T>(this T _) { }

		/// <summary>
		/// Asserts a reference is not <c>null</c>.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T NotNull<T>(this T? obj) where T : class
		{
			if (obj != null)
				return obj;
			else
				throw new NullReferenceException();
		}

		/// <summary>
		/// Asserts a <see cref="Nullable{T}"/> contains a value.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T NotNull<T>(this T? obj) where T : struct => obj!.Value;

		/// <summary>
		/// Pipe. Applies <paramref name="function"/> to <paramref name="argument"/>.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static TResult I<TParameter, TResult>(this TParameter argument, Func<TParameter, TResult> function) =>
			function(argument);
	}
}
