using System.Text;
namespace Victor.Extensions
{
	public static class StringExtensions
	{
		/// <summary>
		/// Indents a string.
		/// </summary>
		/// <param name="text">String to indent</param>
		/// <param name="level">How many indentations to prepend</param>
		/// <param name="indentString">The string to use as the indentation</param>
		public static string Indent(this string text, int level = 1, string indentString = "   ")
		{
			if (level <= 1)
			{
				string result = text;
				for (int i = 0; i < level; i++)
					result = indentString + result;
				return result;
			}
			else
			{
				var builder = new StringBuilder(text.Length + level * indentString.Length);
				for (int i = 0; i < level; i++)
					builder.Append(indentString);
				builder.Append(text);
				return builder.ToString();
			}
		}
	}
}