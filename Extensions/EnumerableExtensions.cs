using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Victor.Extensions
{
	public static class EnumerableExtensions
	{
		/// <summary>
		/// Partitions a sequence into equally sized blocks.
		/// </summary>
		/// <remarks>
		/// The final partition can be smaller than <paramref name="blockSize"/> if number of items in
		/// <paramref name="sequence"/> is not divisible by <paramref name="blockSize"/>.
		/// </remarks>
		/// <param name="sequence">Sequence to partition</param>
		/// <param name="blockSize">Number of elements in a single partition</param>
		/// <returns>A sequence of equally sized <see cref="IEnumerable{T}"/>s</returns>
		public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> sequence, int blockSize)
		{
			return sequence.Select((s, i) => new { Value = s, Index = i })
				.GroupBy(item => item.Index / blockSize, item => item.Value);
		}

		/// <summary>
		/// Returns the first item of a sequence and the rest of the sequence.
		/// </summary>
		/// <exception cref="ArgumentException">Thrown when the sequence is empty</exception>
		public static (T head, IEnumerable<T> tail) GetHeadAndTail<T>(this IEnumerable<T> sequence)
		{
			var enumerator = sequence.GetEnumerator();
			if (enumerator.MoveNext())
			{
				T head = enumerator.Current;
				var tail = EnumerateTail(enumerator);
				return (head, tail);
			}
			else
			{
				throw new ArgumentException("Cannot get head of an empty sequence.");
			}
		}

		private static IEnumerable<T> EnumerateTail<T>(IEnumerator<T> enumerator)
		{
			while (enumerator.MoveNext())
				yield return enumerator.Current;
		}

		/// <summary>
		/// Filters out elements that are <c>null</c> from a sequence.
		/// </summary>
		/// <typeparam name="T">A reference type</typeparam>
		/// <returns>An <see cref="IEnumerable{T}"/> without elements that are <c>null</c></returns>
		public static IEnumerable<T> WhereExists<T>(this IEnumerable<T?> sequence) where T : class =>
			sequence.Where(e => e != null).Select(e => e!);

		/// <summary>
		/// Filters out elements that don't contain a value from a sequence of <see cref="Nullable{T}"/>s.
		/// </summary>
		/// <typeparam name="T">A value type</typeparam>
		/// <returns>An <see cref="IEnumerable{T}"/> without elements that don't contain a value</returns>
		public static IEnumerable<T> WhereExists<T>(this IEnumerable<T?> sequence) where T : struct =>
			sequence.Where(e => e != null).Select(e => e!.Value);

		/// <summary>
		/// Flattens a nested sequence (a sequence of trees).
		/// </summary>
		/// <param name="trees">
		/// An <see cref="IEnumerable{T}"/> where each element is an <see cref="IEnumerable{T}"/> itself
		/// </param>
		/// <returns>A one-dimensional sequence of <typeparamref name="T"/>s</returns>
		public static IEnumerable<T> Flatten<T>(this IEnumerable<T> trees) where T : IEnumerable<T> =>
			trees.Flatten(t => t);

		/// <summary>
		/// Flattens a nested sequence (a sequence of trees).
		/// </summary>
		/// <param name="trees">
		/// An <see cref="IEnumerable{T}"/> where each element can be mapped to an <see cref="IEnumerable{T}"/> itself
		/// </param>
		/// <param name="selector">Maps a <typeparamref name="T"/> to an <see cref="IEnumerable{T}"/></param>
		/// <returns>A one-dimensional sequence of <typeparamref name="T"/>s</returns>
		public static IEnumerable<T> Flatten<T>(this IEnumerable<T> trees, Func<T, IEnumerable<T>> selector) =>
			trees.Concat(trees.SelectMany(n => selector(n).Flatten(selector)));


		/// <summary>
		/// Iterates through a sequence completely, ignoring its elements.
		/// </summary>
		public static void Exhaust(this IEnumerable sequence)
		{
			foreach (var _ in sequence) { }
		}
	}
}
