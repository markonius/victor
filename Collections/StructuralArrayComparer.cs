using System.Collections;
using System.Collections.Generic;

namespace Victor.Collections
{
	/// <summary>
	/// Compares arrays by elements.
	/// </summary>
	public class StructuralArrayComparer<T> : IEqualityComparer<T[]>
	{
		public bool Equals(T[]? x, T[]? y)
		{
			if (x == y)
			{
				return true;
			}
			else if (x == null || y == null)
			{
				return false;
			}
			else
			{
				var sx = (IStructuralEquatable)x;
				var sy = (IStructuralEquatable)y;
				return sx.Equals(sy);
			}
		}

		public int GetHashCode(T[] obj)
		{
			return obj.GetHashCode();
		}
	}
}