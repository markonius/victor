using System.Collections.Generic;

namespace Victor.Collections
{
	/// <summary>
	/// Contains empty instances of some read-only collections.
	/// </summary>
	/// <remarks>
	/// Meant to avoid instantiating the same empty collection multiple times.
	/// </remarks>
	public static class Empty
	{
		public static class List<T>
		{
			public static readonly IReadOnlyList<T> instance = new System.Collections.Generic.List<T>();
		}

		public static class Dictionary<TKey, TValue> where TKey : notnull
		{
			public static readonly IReadOnlyDictionary<TKey, TValue> instance
				= new System.Collections.Generic.Dictionary<TKey, TValue>();
		}
	}
}