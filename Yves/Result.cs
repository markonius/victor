using System;

namespace Victor.Yves;

public interface IResult<out TValue, out TError> : IEither<TValue, TError>
	where TValue : notnull
	where TError : IError
{
	/// <summary>
	/// Perform a different action depending on the type of value.
	/// </summary>
	new void Match(Action<TValue>? ifSuccess = null, Action<TError>? ifFailure = null)
		=> ((IEither<TValue, TError>)this).Match(ifSuccess, ifFailure);

	/// <summary>
	/// Invoke the value depending on the type of value.
	/// </summary>
	new TOut Map<TOut>(Func<TValue, TOut> ifSuccess, Func<TError, TOut> ifFailure)
		=> ((IEither<TValue, TError>)this).Map(ifSuccess, ifFailure);
}

public interface ISuccess<out TValue, out TFailure> : IResult<TValue, TFailure>
	where TValue : notnull
	where TFailure : IError;

public class Success<TValue, TFailure>(TValue value)
	: Either<TValue, TFailure>(value), ISuccess<TValue, TFailure>
	where TValue : notnull
	where TFailure : IError;

public interface IError
{
	string Message { get; }
}

public readonly record struct MessageError(string Message) : IError
{
	public static implicit operator MessageError(string message) => new(message);
}

public readonly record struct ThrowableError(Exception Exception) : IError
{
	public readonly string Message => Exception.Message;

	public static implicit operator ThrowableError(Exception exception) => new(exception);
}

public interface IFailure<out TValue, out TError> : IResult<TValue, TError>
	where TValue : notnull
	where TError : IError;

public class Failure<TValue, TError>(TError error)
	: Either<TValue, TError>(error), IFailure<TValue, TError>
	where TValue : notnull
	where TError : IError;
