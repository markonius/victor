using System;

namespace Victor.Yves;

/// <summary>
/// Contains a value of one of two types.
/// </summary>
public interface IEither<out T1, out T2>
	where T1 : notnull
	where T2 : notnull
{
	/// <summary>
	/// Perform a different action depending on the type of value.
	/// </summary>
	void Match(Action<T1>? ifFirst = null, Action<T2>? ifSecond = null);

	/// <summary>
	/// Invoke the value depending on the type of value.
	/// </summary>
	TOut Map<TOut>(Func<T1, TOut> ifFirst, Func<T2, TOut> ifSecond);

	/// <summary>
	/// Returns value when it is an instance of <typeparamref name="T1"/>. Throws an exception otherwise.
	/// </summary>
	T1 GetFirst();

	/// <summary>
	/// Returns value when it is an instance of <typeparamref name="T2"/>. Throws an exception otherwise.
	/// </summary>
	T2 GetSecond();

	/// <summary>
	/// Returns <c>true</c> if value is an instance of <typeparamref name="T1"/>, <c>false</c> otherwise.
	/// </summary>
	bool IsFirst();

	/// <summary>
	/// Returns <c>true</c> if value is an instance of <typeparamref name="T2"/>, <c>false</c> otherwise.
	/// </summary>
	bool IsSecond();

	public static IEither<T1, T2> First(T1 value)
		=> new Either<T1, T2>(value);
}

/// <summary>
/// Implementation of <see cref="IEither{T1, T2}"/>.
/// </summary>
public class Either<T1, T2> : IEither<T1, T2>
	where T1 : notnull
	where T2 : notnull
{
	private readonly object value;
	private readonly bool isFirst;

	public Either(T1 value)
	{
		this.value = value;
		isFirst = true;
	}

	public Either(T2 value)
	{
		this.value = value;
		isFirst = false;
	}

	public void Match(Action<T1>? ifFirst = null, Action<T2>? ifSecond = null)
	{
		if (isFirst)
			ifFirst?.Invoke((T1)value);
		else
			ifSecond?.Invoke((T2)value);
	}

	public TOut Map<TOut>(Func<T1, TOut> ifFirst, Func<T2, TOut> ifSecond)
	{
		if (isFirst)
			return ifFirst((T1)value);
		else
			return ifSecond((T2)value);
	}

	public Type GetTypeOfValue() => value.GetType();

	public T1 GetFirst() => isFirst ? (T1)value : throw new InvalidOperationException();
	public T2 GetSecond() => !isFirst ? (T2)value : throw new InvalidOperationException();

	public bool IsFirst() => isFirst;
	public bool IsSecond() => !isFirst;

	public static implicit operator Either<T1, T2>(T1 first) => new(first);
	public static implicit operator Either<T1, T2>(T2 second) => new(second);
}
