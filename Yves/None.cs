namespace Victor.Yves;

/// <summary>
/// Nothing. For use as a generic parameter.
/// </summary>
public class None
{
	/// <summary>
	/// Singleton of nothing.
	/// </summary>
	public static readonly None none = new();

	private None() { }
}
